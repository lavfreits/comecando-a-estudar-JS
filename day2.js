/*Qual o seu nome?
- Quantos anos você tem?
- Qual linguagem de programação você está estudando?

À medida que as perguntas forem sendo feitas, a pessoa que estiver usando o programa deve responder cada uma delas.

No final, o sistema vai exibir a mensagem:

"Olá [nome], você tem [idade] anos e já está aprendendo [linguagem]!"*/

const nome = prompt("Qual o seu nome?");
const idade = prompt("Qual a sua idade?");
const linguagem = prompt("Qual linguagem de programação você está aprendendo?");
const msg = '"Olá ${nome}, você tem ${idade} anos e já está aprendendo ${linguagem}!"!';

alert(msg);

/*Você gosta de estudar [linguagem]? Responda com o número 1 para SIM ou 2 para NÃO.

E aí, dependendo da resposta, ele deve mostrar uma das seguintes mensagens:

1 > Muito bom! Continue estudando e você terá muito sucesso.
2 > Ahh que pena... Já tentou aprender outras linguagens?

if (resposta == 1){
    // dê a resposta positiva
}
if (resposta == 2){
    // dê a resposta negativa
}*/

const gosta = prompt("Você gosta de estudar ${linguagem}? Responda com o número 1 para SIM ou 2 para NÃO.");
if (gosta == 1){
    alert("Muito bom! Continue estudando e você terá muito sucesso.");
    // dê a resposta positiva
}
if (gosta == 2){
    alert("Ahh que pena... Já tentou aprender outras linguagens?");
        // dê a resposta negativa
}